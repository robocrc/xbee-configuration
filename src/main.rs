use std::io::{Read, Write};
use std::process;
use std::time::Duration;
use std::fs::{File, OpenOptions};

use chrono::prelude::*;

use anyhow::Context;
use console::style;
use dialoguer::{theme::ColorfulTheme, Confirm, Input, MultiSelect, Select};
use env_logger::Env;
use log::*;
use serialport::{SerialPort, SerialPortType};

fn main() -> anyhow::Result<()> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let mut file = OpenOptions::new().append(true).create(true).open("export.txt").context("Can't create file")?;

    let ports = serialport::available_ports().context("No serial port")?;
    let mut ports = ports
        .into_iter()
        .filter(|p| matches!(p.port_type, SerialPortType::UsbPort(_)))
        .collect::<Vec<_>>();
    if ports.is_empty() {
        error!("No serial ports");
        process::exit(1);
    }
    debug!("Found {} serial ports", ports.len());
    let chosen: Vec<usize> = MultiSelect::with_theme(&ColorfulTheme::default())
        .with_prompt("What do you want to program?")
        .items_checked(&[("CrcDuino", true), ("CrcConnect", true)])
        .interact()
        .expect("Could not interact with console");

    if chosen.len() > ports.len() {
        error!("Not enough serial ports");
        process::exit(1);
    }

    let crc_duino = if chosen.contains(&0) {
        let selection = Select::with_theme(&ColorfulTheme::default())
            .with_prompt("What port should be used for CrcDuino?")
            .items(&ports.iter().map(|p| &p.port_name).collect::<Vec<_>>())
            .default(0)
            .interact()
            .expect("Could not interact on console");
        Some(ports.remove(selection).port_name)
    } else {
        None
    };
    let crc_connect = if chosen.contains(&1) {
        let selection = Select::with_theme(&ColorfulTheme::default())
            .with_prompt("What port should be used for CrcConnect?")
            .items(&ports.iter().map(|p| &p.port_name).collect::<Vec<_>>())
            .default(0)
            .interact()
            .expect("Could not interact on console");
        Some(ports.remove(selection).port_name)
    } else {
        None
    };
    let mut id: u16 = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("What is the corresponding kit serial value? (ex: 20 for CrcDuino020)")
        .interact()
        .expect("Could not interact on console");

    match (crc_duino, crc_connect) {
        (Some(crc_duino), Some(crc_connect)) => loop {
            let (mut duino, duino_sh, duino_sl) = find(&crc_duino)?;
            let (mut connect, connect_sh, connect_sl) = find(&crc_connect)?;
            debug!(
                "Duino is (SH, SL) = ({}, {}), Connect is (SH, SL) = ({}, {})",
                duino_sh, duino_sl, connect_sh, connect_sl
            );

            info!("Copy following lines to Excel (https://docs.google.com/spreadsheets/d/1fs3_v5r2cwQkXSmtPCOl7vPbRlu6O9D9rvMpuUQjm_M/edit#gid=0):");
            prog(
                &mut file,
                &mut *duino,
                (&duino_sh, &duino_sl),
                Some((&connect_sh, &connect_sl)),
                id,
                true,
            )?;
            prog(
                &mut file,
                &mut *connect,
                (&connect_sh, &connect_sl),
                Some((&duino_sh, &duino_sl)),
                id,
                false,
            )?;

            id += 1;
            if !Confirm::new()
                .with_prompt(format!(
                    "Program next pair (CrcDuino{id:03}/CrcConnect{id:03})?"
                ))
                .default(true)
                .interact()
                .unwrap()
            {
                break;
            }
        },
        (Some(crc_duino), None) => {
            let (mut duino, sh, sl) = find(&crc_duino)?;
            info!("Copy following line to Excel (https://docs.google.com/spreadsheets/d/1fs3_v5r2cwQkXSmtPCOl7vPbRlu6O9D9rvMpuUQjm_M/edit#gid=0):");
            prog(&mut file, &mut *duino, (&sh, &sl), None, id, true)?;
        }
        (None, Some(crc_connect)) => {
            let (mut connect, sh, sl) = find(&crc_connect)?;
            info!("Copy following line to Excel (https://docs.google.com/spreadsheets/d/1fs3_v5r2cwQkXSmtPCOl7vPbRlu6O9D9rvMpuUQjm_M/edit#gid=0):");
            prog(&mut file, &mut *connect, (&sh, &sl), None, id, false)?;
        }
        _ => {}
    }
    Ok(())
}

fn find(path: &str) -> anyhow::Result<(Box<dyn SerialPort>, String, String)> {
    debug!("Trying port {}", path);
    let mut port = try_baud_rate_api(path, 9600)
        .or_else(|| try_baud_rate_api(path, 115200))
        .context("No available XBee")?;

    port.write_all(b"ATSH\r")
        .context("Could not communicate on TTY port")?;
    port.write_all(b"ATSL\r")
        .context("Could not communicate on TTY port")?;
    let mut sh = Vec::with_capacity(8);
    let mut sl = Vec::with_capacity(8);

    let mut cnt = 0;
    while cnt < 2 {
        let mut tmp = [0x00_u8; 1];

        match port.read(&mut tmp).context("Could not read on TTY port")? {
            1 if tmp[0] == b'\r' => {
                cnt += 1;
            }
            1 if cnt == 0 => sh.push(tmp[0]),
            1 if cnt == 1 => sl.push(tmp[0]),
            _ => {}
        }
    }

    Ok((
        port,
        String::from_utf8(sh).expect("XBee returned non-UTF-8 SH value?"),
        String::from_utf8(sl).expect("XBee returned non-UTF-8 SL value?"),
    ))
}

fn prog<S: SerialPort + ?Sized>(
    file: &mut File,
    port: &mut S,
    addr: (&str, &str),
    other: Option<(&str, &str)>,
    id: u16,
    is_duino: bool,
) -> anyhow::Result<()> {
    let name = format!(
        "{}{:03}",
        if is_duino { "CrcDuino" } else { "CrcConnect" },
        id
    );
    let hp = id % 8;
    let network = id + 2000;

    let mut commands = Vec::from([
        "ATRE".into(),
        format!("ATHP {}", hp),
        format!("ATID {}", network),
        format!("ATPL {}", if is_duino { 4 } else { 2 }),
        "ATRR 5".into(),
        "ATCE 2".into(),
        "ATTO 40".into(),
        format!("ATNI {}", name),
        "ATNO 4".into(),
        format!("ATBD {}", if is_duino { 3 } else { 7 }),
        "ATAP 2".into(),
        "ATWR".into(),
        "ATCN".into(),
    ]);
    if let Some((sh, sl)) = &other {
        commands.insert(1, format!("ATDH {}", sh));
        commands.insert(1, format!("ATDL {}", sl));
    }

    for command in commands {
        port.write_all(command.as_bytes()).context("Could not write to port")?;
        port.write_all(b"\r").context("Could not write to port")?;
        debug!("{}", command);
        let mut out = [0x00_u8; 3];
        port.read_exact(&mut out).context("Could not write to port")?;
        assert_eq!(&out, b"OK\r");
    }

    let local_date: DateTime<Local> = Local::now();
    let out = format!(
        "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
        local_date.format("%Y-%m-%d"),
        "!MasterConfig_v1_1.xml",
        name,
        network,
        hp,
        addr.0,
        addr.1,
        other.map_or("??\t??".into(), |(sh, sl)| format!("{}\t{}", sh, sl)),
        "FFFFFFFFFFF7FFFF");
    
    writeln!(file, "{}", out).context("Could not write to file")?;

    println!(
        "{}",
        style(&out)
        .magenta()
        .bold()
    );
    Ok(())
}

fn try_baud_rate_api(path: &str, baud: u32) -> Option<Box<dyn SerialPort>> {
    let mut port = serialport::new(path, baud)
        .timeout(Duration::from_millis(100))
        .open()
        .expect("Could not open port");

    port.write_all(b"+++").unwrap();
    std::thread::sleep(Duration::from_secs(2));
    let mut out = [0x00_u8; 3];
    let _ = port.read_exact(&mut out).ok();
    if &out != b"OK\r" {
        return None;
    }
    info!("Found with baud: {}", baud);
    Some(port)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn framing() {
        const MSG: &[u8] = &[
            0x10, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xDA, 0x9D, 0x23, 0xA6, 0xB9, 0x00, 0x00,
            0x48, 0x65, 0x6C, 0x6C, 0x6F,
        ];
        const ENCODED: &[u8] = &[
            0x7E, 0x00, 0x13, 0x10, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xDA, 0x9D, 0x23, 0xA6,
            0xB9, 0x00, 0x00, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x0C,
        ];
        let mut buf = Vec::new();
        frame(&mut buf, false, MSG).expect("Could not decode frame");
        assert_eq!(buf, ENCODED);
        let out = decode_frame(&*buf, false).expect("Could not decode frame");
        assert_eq!(out, MSG);
    }

    #[test]
    fn framing2() {
        const MSG: &[u8] = &[
            0x91, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xDA, 0x9D, 0x05, 0x00, 0x00, 0xE8, 0xE8, 0x00,
            0x11, 0xC1, 0x05, 0x01, 0x48, 0x65, 0x6C, 0x6C, 0x6F,
        ];
        let mut buf = Vec::new();
        frame(&mut buf, true, MSG).expect("Could not decode frame");
        let out = decode_frame(&*buf, true).expect("Could not decode frame");
        assert_eq!(out, MSG);
    }

    #[test]
    fn escaping() {
        const MSG: &[u8] = &[
            0x17, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xAD, 0x14, 0x2E, 0xFF, 0xFE, 0x02, 0x4E,
            0x49,
        ];
        const ENCODED: &[u8] = &[
            0x7E, 0x00, 0x0F, 0x17, 0x01, 0x00, 0x7D, 0x33, 0xA2, 0x00, 0x40, 0xAD, 0x14, 0x2E,
            0xFF, 0xFE, 0x02, 0x4E, 0x49, 0x6D,
        ];
        let mut buf = Vec::new();
        frame(&mut buf, true, MSG).expect("Could not decode frame");
        assert_eq!(buf, ENCODED);
    }
}
